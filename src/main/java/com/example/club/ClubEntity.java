package com.example.club;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;
import java.util.UUID;

@Entity(name="club")
@Data
@NoArgsConstructor
public class ClubEntity {
    @Id
    String id;

    @Column
    String name;


    @Column
    @ElementCollection
    List<String> members;

    public ClubEntity(String name, List<String> members) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.members = members;
    }

    public void addMember(String newMemberId) throws ClubAlreadyContainsThisMemberException {
        if(members.contains(newMemberId)){
            throw new ClubAlreadyContainsThisMemberException("THIS USER IS ALREADY A MEMBER OF THIS CLUB");
        }
        members.add(newMemberId);
    }

    public void removeAllMembers(){
        members.clear();
    }

    public void removeMember(String memberId){
        members.remove(memberId);
    }
}
