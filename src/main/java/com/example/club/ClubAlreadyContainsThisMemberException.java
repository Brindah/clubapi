package com.example.club;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN, reason = "MEMBER_ALREADY_ADDED")
public class ClubAlreadyContainsThisMemberException extends Exception {
    public ClubAlreadyContainsThisMemberException(String s) {
        super(s);
    }
}
