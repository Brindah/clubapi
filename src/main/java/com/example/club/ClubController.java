package com.example.club;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor
@RequestMapping("clubs")
public class ClubController {

    @Autowired
    ClubRepository clubRepository;
    @Autowired
    ClubService clubService;

    @GetMapping("/{clubId}")
    public ClubDTO getClubById(@PathVariable("clubId") String clubId) throws ClubNotFoundException {
        return toDto(clubService.getClubById(clubId));
    }

    @GetMapping("/all_clubs")
    public List<ClubDTO> getAllClubs() {
        return clubService.findAllClubs()
                .stream()
                .map(ClubController::toDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/add_member/{clubId}")
    public ClubDTO addMember(@PathVariable("clubId") String clubId,
                             @RequestParam(value = "memberId") String memberId) throws ClubNotFoundException, ClubAlreadyContainsThisMemberException {
        return toDto(clubService.addMember(clubId,memberId));
    }

    @PostMapping("/create_club")
    public ClubDTO createClub(@RequestParam(value = "clubName") String clubName) throws ClubNotFoundException {
        return toDto(clubService.createClub(clubName));
    }

    @PutMapping("/update_club_name/{clubId}")
    public ClubDTO updateClubName(@PathVariable("clubId") String clubId,
                                  @RequestParam(value = "newName") String newName) throws ClubNotFoundException {
        return toDto(clubService.updateClubName(clubId, newName));
    }

    @GetMapping("/get_club_by_name")
    public ClubDTO getClubByName(@RequestParam("clubName") String clubName) throws ClubNotFoundException {
        return toDto(clubService.findByName(clubName));
    }

    @DeleteMapping("/delete_club/{clubId}")
    public void deleteClub(@PathVariable("clubId") String clubId) throws ClubNotFoundException {
        clubService.deleteClub(clubId);
    }

    @DeleteMapping("/remove_member/{clubId}")
    public void removeMember(@PathVariable("clubId") String clubId,
                           @RequestParam("memberId") String memberId) throws ClubNotFoundException {
        clubService.removeMember(clubId, memberId);
    }

    public static ClubDTO toDto(ClubEntity clubEntity) {
        return new ClubDTO(
                clubEntity.getId(),
                clubEntity.getName(),
                clubEntity.getMembers());
    }
}
