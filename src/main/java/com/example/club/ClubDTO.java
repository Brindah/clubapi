package com.example.club;

import lombok.Value;

import java.util.List;

@Value
public class ClubDTO {
    String id;
    String name;
    List<String> members;
}
