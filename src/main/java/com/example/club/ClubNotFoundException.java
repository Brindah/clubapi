package com.example.club;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "CLUB_NOT_FOUND")
public class ClubNotFoundException extends Exception {
    public ClubNotFoundException(String s){
        super(s);
    }
}
