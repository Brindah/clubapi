package com.example.club;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClubRepository extends JpaRepository<ClubEntity, String> {
    ClubEntity findClubEntityByNameContaining(String name);
}
