package com.example.club;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ClubService {

    ClubRepository clubRepository;


    public ClubEntity getClubById(String clubId) throws ClubNotFoundException {
        return clubRepository.getById(clubId);
    }

    public List<ClubEntity> findAllClubs() {
        return clubRepository.findAll();
    }

    public ClubEntity findByName(String name) throws ClubNotFoundException {
        return clubRepository.findClubEntityByNameContaining(name);
    }

    public ClubEntity createClub(String name) throws ClubNotFoundException {
        ClubEntity club = new ClubEntity(name, null);
        return clubRepository.save(club);
    }

    public ClubEntity updateClubName(String clubId, String newName) throws ClubNotFoundException {
        ClubEntity club = clubRepository.getById(clubId);
        club.setName(newName);
        return clubRepository.save(club);
    }

    public ClubEntity addMember(String clubId, String memberId) throws ClubNotFoundException, ClubAlreadyContainsThisMemberException {
     ClubEntity club= clubRepository.getById(clubId);
        club.addMember(memberId);
        return clubRepository.save(club);
    }

    public void deleteClub(String clubId) throws ClubNotFoundException {
        ClubEntity club = clubRepository.getById(clubId);
        clubRepository.delete(club);
    }

    public void removeMember(String clubId, String memberId) {
        ClubEntity club = clubRepository.getById(clubId);
        club.removeMember(memberId);
    }
}
